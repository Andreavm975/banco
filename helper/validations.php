<?php
function validateRegister(){
    $validate = true;
    if(!validateName($_POST["name"] . $_POST["surname"])){
        $_POST['errorName'] = 'Introduce un nombre válido';
        $validate = false;
    }
    if(!validateTelf($_POST["telf"])){
        $_POST['errorTelf'] = 'Introduce un número de teléfono correcto';
        $validate = false;
    }

    if(!validateDNI($_POST["dni"])){
        $_POST['errorDNI'] = 'Introduce un DNI válido';
        $validate = false;
    }

    if(!validateEmail($_POST["email"])){
        $_POST['errorEmail'] = 'Introduce un email válido';
        $validate = false;
    }

    if(!validatePassword($_POST["password"])){
        $_POST['errorPassword'] = 'La contraseña debe contener al menos una mayúscula, una minúscula, un número y un carácter especial.';
        $validate = false;
    }

    return $validate;
}

function validateName($name){
    return preg_match("/^[a-zA-Z-' ]*$/",$name);
}

function validateTelf($telefono){
    if(strlen($telefono) == 9){
        return true;
    }else{
        return false;
    }
}

function validateDNI($dni){
    if(strlen($dni) == 9){
        $nums = substr($dni, 0,8);
        if(calculateLetter($nums) == substr($dni, 8,1)){
            return true;
        } else{
            return false;
        }
    } else{
        return false;
    }
}

function calculateLetter($dni){
    return substr("TRWAGMYFPDXBNJZSQVHLCKE", $dni%23, 1);
}

function validateEmail($email){
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function validatePassword($password){
    $uppercase = preg_match('@[A-Z]@', $password);
    $lowercase = preg_match('@[a-z]@', $password);
    $number    = preg_match('@[0-9]@', $password);
    $specialChars = preg_match('@[^\w]@', $password);

    if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
        return true;
    } else{
        return false;
    }
}