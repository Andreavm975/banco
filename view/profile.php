<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title>Perfil - Banco Jones</title>
</head>
<body>

<?php
session_start();
if (isset($_SESSION['cliente'])){?>

<nav>
    <ul>
        <li>
            <a href="init.php"><div class = "logo"></div></a>
        </li>
        <li>
            <a href="profile.php">Perfil</a>
        </li>
        <li>
            <a href="transfer.php">Transferencia</a>
        </li>
        <li>
            <a href="query.php">Movimientos</a>
        </li>

        <li>
            <a href="logout.php">Logout</a>
        </li>
    </ul>
</nav>
<main>
<?php

include '../model/Cliente.php';
include '../model/ClienteModel.php';
?>
    <form action="../controller/controller.php" method="post" enctype="multipart/form-data">
    <label for="name">Nombre
            <input id="name" name="name" type="text" value="<?php echo unserialize($_SESSION['cliente'])->getNombre();?>" readonly>
        </label>
        <label for="surname">Apellidos
            <input id="surname" name="surname" type="text" value="<?php echo unserialize($_SESSION['cliente'])->getApellidos(); ?>"  readonly>
        </label>
        <label for="bornDate">Fecha de nacimiento
            <input id="bornDate" name="bornDate" type="date" value="<?php echo unserialize($_SESSION['cliente'])->getFechaNacimiento()?>" readonly>
        </label>
        <label for="gender">Género
        <input id="gender" name="genre" type="text" value="<?php echo unserialize($_SESSION['cliente'])->getSexo()?>" readonly>
        </label>
        <label for="dni">DNI
            <input id="dni" name="dni" type="text" value="<?php echo unserialize($_SESSION['cliente'])->getDni()?>" readonly>
        </label>
        <label for="telf">Teléfono
            <input id="telf" name="telf" type="tel" value="<?php echo unserialize($_SESSION['cliente'])->getTelefono()?>">
        </label>
        <label for="email">Email
            <input id="email" name="email" type="email" value="<?php echo unserialize($_SESSION['cliente'])->getEmail()?>">
        </label>
        <label for="password">Contraseña
            <input id="password" name="password" type="password">
        </label>
        <label for="upload">
            <?php if(getImage($_SESSION['cliente'])=="") echo "<br/><img src='https://cdn.onlinewebfonts.com/svg/img_522887.png'/>";
             else{
                 $data=getImage($_SESSION['cliente']);
                 ob_start();
                 fpassthru($data);
                 $im = ob_get_contents();
                 ob_end_clean();

                 echo "<br/><img src='data:image/*;base64," . base64_encode($im) . "'/>";

             } ?>
        </label>
        <input type="file" name="upload" id="upload" accept="image/png, .jpeg, .jpg, image/gif" style="display: none"><br/>
        <input type="hidden" value="profile" name="control">
        <input name="submit" type="submit">
    </form>

</main>


<?php
}else{
header("Location: login.php");

}?>
</body>
</html>