<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title>Logout - Banco Jones</title>
</head>
<body>
 <?php
    session_start();
    unset($_SESSION["cliente"]);
    session_destroy();
    ?>
    <main>
        <a href="init.php"><div class = "logo"></div></a>

        <h3>
            ¡Esperamos verte pronto!
        </h3>


    </main>
</body>
</html>