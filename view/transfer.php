<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title>Transfer - Banco Jones</title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['cliente'])){?>


<nav>
    <ul>
        <li>
            <a href="init.php"><div class = "logo"></div></a>
        </li>
        <li>
            <a href="profile.php">Perfil</a>
        </li>
        <li>
            <a href="transfer.php">Transferencia</a>
        </li>
        <li>
            <a href="query.php">Movimientos</a>
        </li>

        <li>
            <a href="logout.php">Logout</a>
        </li>
    </ul>
</nav>
    <main>
<form action="../controller/controller.php" method="post">
    <select name="cuentas">

        <?php
        require_once('../model/CuentaModel.php');
        require_once('../model/Cliente.php');
        $accounts=getAccounts(unserialize($_SESSION['cliente'])->getDni());
        for ($i=0; $i<sizeof($accounts) ;$i++){?>
            <option ><?php echo $accounts[$i]["cuenta"] ?></option>
        <?php }?>
    </select>
    <label>Cuenta destino <input name="cuenta_destino" type="text" /><label>
    <label>Cantidad <input name="cantidad" type="text" /></label>
    <input class="transferSubmit" name="submit" type="submit" value="Seleccionar"/>
    <input name="control" type="hidden" value="transfer"/>

</form>

<?php
}else{
header("Location: login.php");

}?>
    </main>
</body>
</html>