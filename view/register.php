<html lang = "es">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title><?php echo _('Crea tu cuenta');?> - Banco Jones</title>
</head>
<body>
    <main>
        <?php require_once ('header.php')?>
        <h3>
            <?php echo _('Crea tu cuenta');?>
        </h3>
        <form action="../controller/controller.php" method="post">
            <label for="name">Nombre
                <input id="name" name="name" type="text">
            </label>
            <label for="surname">Apellidos
                <input id="surname" name="surname" type="text">
            </label>
            <label for="bornDate">Fecha de nacimiento
                <input id="bornDate" name="bornDate" type="date">
            </label>
            <label for="gender">Género
                <input type="radio" id="gender" name="gender" value="F">Mujer
                <input type="radio" id="gender" name="gender" value="M">Hombre
                <input type="radio" id="gender" name="gender" value="O">Otro
            </label>
            <label for="telf">Teléfono
                <input id="telf" name="telf" type="tel">
            </label>
            <label for="dni">DNI
                <input id="dni" name="dni" type="text">
            </label>
            <label for="email">Email
                <input id="email" name="email" type="email">
            </label>
            <label for="password">Contraseña
                <input id="password" name="password" type="password">
            </label>
            <input name="control" type="hidden" value="register">
            <input name="submit" type="submit" value="submit">
        </form>
        <?php
            if (isset($_POST['errorName'])) echo $_POST['errorName'] . '</br>';
            if (isset($_POST['errorTelf'])) echo $_POST['errorTelf'] . '</br>';
            if (isset($_POST['errorDNI'])) echo $_POST['errorDNI'] . '</br>';
            if (isset($_POST['errorEmail'])) echo $_POST['errorEmail'] . '</br>';
            if (isset($_POST['errorPassword'])) echo $_POST['errorPassword'] . '</br>';
        ?>
    </main>
</body>
</html>