<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title>Banco Jones</title>
</head>
<body>

<?php
session_start();
if (isset($_SESSION['cliente'])){?>

<nav>
    <ul>
        <li>
            <a href="init.php"><div class = "logo"></div></a>
        </li>
        <li>
            <a href="profile.php">Perfil</a>
        </li>
        <li>
            <a href="transfer.php">Transferencia</a>
        </li>
        <li>
            <a href="query.php">Movimientos</a>
        </li>

        <li>
            <a href="logout.php">Logout</a>
        </li>
    </ul>
</nav>
<main>

    <form action="../controller/controller.php" method="post">
        <input name="submit" type="submit" value="Crear cuenta"/>
        <input name="control" type="hidden" value="create"/>
    </form>


    <form action="../controller/controller.php" method="post">
        <select name="cuentas">

            <?php
            require_once('../model/CuentaModel.php');
            require_once('../model/Cliente.php');
            session_start();
            $accounts=getAccounts(unserialize($_SESSION['cliente'])->getDni());
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="select_account"/>
    </form>

</main>


<?php
}else{
header("Location: login.php");

}?>


</body>
</html>
