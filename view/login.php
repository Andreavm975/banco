<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <title>Login - Banco Jones</title>
</head>
<body>
<main>

    <h3>Login</h3>
    <form action="../controller/controller.php" method="post">
        <label for="DNI">DNI
            <input id="DNI" name = "dni" type = "text">
        </label>
        <label for="password">Password
            <input id = "password" name = "password" type = "password">
        </label>
        <input name = "control" type = "hidden" value ="login">
        <input class="loginSubmit" name = "submit" value="Submit" type="submit">
    </form>
</main>
</body>
</html>