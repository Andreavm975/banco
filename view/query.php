<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../style/style.css">
    <script rel="script" src="../js/index.js"></script>
    <title>Lista de movimientos - Banco Jones</title>
</head>
<body>
<?php
session_start();
if (isset($_SESSION['cliente'])){?>


<nav>
    <ul>
        <li>
            <a href="init.php"><div class = "logo"></div></a>
        </li>
        <li>
            <a href="profile.php">Perfil</a>
        </li>
        <li>
            <a href="transfer.php">Transferencia</a>
        </li>
        <li>
            <a href="query.php">Movimientos</a>
        </li>

        <li>
            <a href="logout.php">Logout</a>
        </li>
    </ul>
</nav>
<main>
    <form action="../controller/controller.php" method="post">
        <select name="cuentas">

            <?php
            require_once('../model/CuentaModel.php');
            require_once('../model/Cliente.php');
            $accounts=getAccounts(unserialize($_SESSION['cliente'])->getDni());
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="query"/>
    </form>


    <?php
    if (isset($_SESSION['saldo'])) {
        echo "Saldo total " . $_SESSION['saldo'] . ' €<br/>';
    }
    if (isset($_SESSION['lista'])) {
        $movimientos=$_SESSION['lista'];
        echo '<table class="default" rules="all" frame="border">';
        echo '<tr>';
        echo '<th>Origen</th>';
        echo '<th>Destino</th>';
        echo '<th>Hora</th>';
        echo '<th>Cantidad</th>';
        echo '</tr>';
        for ($i=0;$i<count($movimientos);$i++){
            echo '<tr>';
            echo '<td>'.$movimientos[$i]['id_origen'].'</td>';
            echo '<td>'.$movimientos[$i]['id_destino'].'</td>';
            echo '<td>'.$movimientos[$i]['fecha'].'</td>';
            echo '<td>'.$movimientos[$i]['cantidad'].'</td>';
            echo '</tr>';
        }
        echo '</table>';

    }

    }else{
        header("Location: login.php");

    }?>
</main>
</body>
</html>